let testFullName = require('./../stringProblem4.cjs');

let humanName = {
    "first_name": "JoHN", 
    "middle_name": "doe", 
    "last_name": "SMith"
};

let defaultArr = ["first_name", "middle_name", "last_name"];

let result = testFullName(defaultArr, humanName);
console.log(result);

