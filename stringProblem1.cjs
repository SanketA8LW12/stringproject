function convertToNumericFormat(value){
    if((typeof value) === 'string'){
        let stringNum = value.replace(/[^.0-9-]/g, "");

        if(isNaN(stringNum)){ // if there are two decimals in string
            return 0;
        } 

        // to handle the case if string = "abc";
        if(stringNum.length === 0){
            return 0;
        }
        
        let number = parseFloat(stringNum);
        return number;
    }
    return 0;
}

module.exports = convertToNumericFormat;