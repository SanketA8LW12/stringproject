function fullName(defaultArr, obj){
    let name = "";
    for(let index in defaultArr){
        if(defaultArr[index] in obj){
            name += obj[defaultArr[index]];
            name += " ";
        }
    }
    // below code to convert string into title case
    name = name.toLowerCase();
    name = name.split(' ');
    for(let index = 0; index < name.length; index++){
        name[index] = name[index].charAt(0).toUpperCase() + name[index].slice(1);
    }
    name = name.join(' ');
    
    return name;
}

module.exports = fullName;