function arrayToString(stringArr){
    let stringName = "";
    if(stringArr.length === 0){ // handles the case if passed array is empty
        return stringName;
    }
    for(let index = 0; index < stringArr.length; index++){
        stringName += stringArr[index];
        if(index != stringArr.length - 1){
            stringName += " ";
        }
    }
    stringName += ".";
    return stringName;
    
}

module.exports = arrayToString;
