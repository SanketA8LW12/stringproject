function splitIpAddress(stringIP) {
    let ipArray = stringIP.split('.');

    if (ipArray.length !== 4) { // handles case if other charachter detected;
        return [];
    }

    let IpArrayNumberic = [];

    for (let index = 0; index < ipArray.length; index++) {


        IpArrayNumberic[index] = +ipArray[index];

        if (Number.isNaN(IpArrayNumberic[index])) { // handles the case if this is passed"111.abc.123.143" 
            return [];
            
        }

    }

    // checking the range from 0 to 255 to be valid ip input
    for (let index = 0; index < IpArrayNumberic.length; index++) {
        if (IpArrayNumberic[index] < 0 || IpArrayNumberic[index] > 255) {
            return [];
        }
    }

    return IpArrayNumberic;
}

module.exports = splitIpAddress;

