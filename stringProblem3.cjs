const month = ["January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"];

function getMonthFromDate(date) {
    if ((typeof date) === 'string') {

        let stringDate = date.replace(/[^0-9]/g, "");
    
        let stringMonth;

        if (stringDate.length === 7) { // if date is passed as "20/5/2021"
            stringMonth = stringDate.charAt(2);
        }

        if (stringDate.length === 8) { // if date is passed as "20/11/2021"
            stringMonth = stringDate.charAt(2);
            stringMonth += stringDate.charAt(3);
        }

        let numDate = parseInt(stringMonth);

        if (numDate < 13 && numDate > 0) {
            return month[numDate - 1];
        }
    }

    return "Invalid date. Please check the date you entered.";
}

module.exports = getMonthFromDate;


